# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|

  # Vagrant manages setup
  config.vm.box = "trusty64"
  config.vm.box_check_update = false
  config.vm.hostname = "vagrant"

  # network specification
  config.vm.network "forwarded_port", guest: 80, host: 5000
  config.vm.network "private_network", ip: "192.168.50.100"

  # ssh
  config.ssh.forward_agent = true
  config.ssh.password = "vagrant"
  config.ssh.username = "vagrant"

  # sync folder
  config.vm.synced_folder "/Users/bjmin/Documents/repo", "/home/vagrant/repo",
    :type => "nfs",
    :mount_options => ['nolock,vers=3,udp,noatime,actimeo=1']

  # provider
  config.vm.provider :virtualbox do |vb|
    vb.customize [
      'modifyvm', :id,
      '--natdnshostresolver1', 'on',
      '--name', 'dev-machine',
      '--memory', '1024',
    ]
  end

  # provision
  config.vm.provision "shell", path: "./sh/source.sh"
  config.vm.provision "shell", path: "./sh/apt-get.sh"
  config.vm.provision "shell", path: "./sh/git.sh"
  config.vm.provision "shell", path: "./sh/vim.sh"
  config.vm.provision "shell", path: "./sh/zsh.sh"
  config.vm.provision "shell", path: "./sh/nginx.sh"
  config.vm.provision "shell", path: "./sh/nodejs.sh"
  config.vm.provision "shell", path: "./sh/python.sh"
  config.vm.provision "shell", path: "./sh/ruby.sh"

end
