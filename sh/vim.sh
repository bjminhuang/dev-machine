#!/bin/bash

echo "install vim"
cd ~
git clone https://github.com/vim/vim.git
cd vim/src
make distclean
make
sudo make install
cd ~
rm -rf vim
echo "done!"
